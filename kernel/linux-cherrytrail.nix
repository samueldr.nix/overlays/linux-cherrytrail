# Based on <nixpkgs/pkgs/os-specific/linux/kernel/linux-4.15.nix>
{ stdenv, buildPackages, hostPlatform, fetchFromGitHub, perl, buildLinux, ... } @ args:

with stdenv.lib;

buildLinux (args // rec {
  # Version is here: https://github.com/jwrdegoede/linux-sunxi/blob/master/Makefile#L1
  version = "5.7.0-rc1";

  # Cleans the source tree...
  src = stdenv.mkDerivation {
    name = "linux-cherrytrail-src-patched";

    # From the author's tree...
    src = fetchFromGitHub {
      owner = "jwrdegoede";
      repo = "linux-sunxi";
      rev = "2cee5c527d361d8db248d36332dbfb5ec5f9fcd7";
      sha256 = "0k2jnnhbsa780nq32mfgw6p7v5xqi5lngw8kpy8bhjswk4dvdi9w";
    };

    dontBuild = true;
    installPhase = ''
      rm .config
      cp -prf ./ $out/
    '';
    fixupPhase = ''
      true
    '';
  };

  # modDirVersion needs to be x.y.z, will automatically add .0 if needed
  modDirVersion = concatStrings (intersperse "." (take 3 (splitString "." "${version}.0")));

  # branchVersion needs to be x.y
  extraMeta.branch = concatStrings (intersperse "." (take 2 (splitString "." version)));

  extraConfig = ''
    ## Breaks the build
    #PHY_QCOM_USB_HS n

    ## The USB_XHCI build is broken as a module, as of right now.
    ## https://www.spinics.net/lists/linux-usb/msg151039.html
    #USB y
    #USB_XHCI_HCD y
    #USB_XHCI_PCI y

    # I2C stuff needed for cherry trail...
    I2C y
    I2C_DESIGNWARE_CORE y
    I2C_DESIGNWARE_PLATFORM y
    I2C_DESIGNWARE_PCI y
    I2C_DESIGNWARE_BAYTRAIL y
  ''
  +
  # See:
  # https://bugs.freedesktop.org/show_bug.cgi?id=96571
  ''
    PWM y
    PWM_CRC y
    INTEL_SOC_PMIC y
    DRM_I915 m
  ''
  +
  # hci_h5.c is built for HCIUART_3WIRE.
  # The h5 protocol is used for rtl8723bs.
  ''
    BT_HCIUART_3WIRE y
  ''
  +
  # Patch over dropped configuration options
  ''
    DONGLE? y
    IRDA_ULTRA? y
  ''
  ;

} // (args.argsOverride or {}))
