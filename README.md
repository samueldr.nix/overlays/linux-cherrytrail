Atom Cherry Trail Overlay
=========================

Overlay including fixes for the common Atom Cherry Trail platforms.

  * kernel from : https://github.com/jwrdegoede/linux-sunxi

Tested hardware
---------------

  * Chuwi Hi10 Pro Z8350 (HQ64)

