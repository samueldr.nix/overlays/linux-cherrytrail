self: super:

with super.lib;

let
  callPackage = self.callPackage;
  mkIf = self.lib.mkIf;
  kernelPatches = self.kernelPatches;
  platform = self.platform;
in
{
  linux_cherrytrail = callPackage ./kernel/linux-cherrytrail.nix {
    kernelPatches = [
      kernelPatches.bridge_stp_helper
      # FIXME for 5.7
      #kernelPatches.modinst_arg_list_too_long
    ];
  };
  linuxPackages_cherrytrail = self.linuxPackagesFor self.linux_cherrytrail;
}
